import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatabaseReaderService } from '../core/services/database-reader/database-reader.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private db: DatabaseReaderService
  ) { }

  ngOnInit(): void {
    this.db.init();
  }

}
