import { TestBed } from '@angular/core/testing';

import { DatabaseReaderService } from './database-reader.service';

describe('DatabaseReaderService', () => {
  let service: DatabaseReaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatabaseReaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
