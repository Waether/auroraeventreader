import { Injectable } from '@angular/core';
import { ipcMainResponse } from '../../../models/ipcMainResponse';
import { ElectronService } from '../electron/electron.service';

@Injectable({
  providedIn: 'root'
})
export class DatabaseReaderService {

  constructor(
    private electron: ElectronService
  ) {
  }

  init(): void {
    this.execQuery('SELECT * FROM DIM_Ruin')
      .then(result => {
        console.log('[App] Success: ', result);
      })
      .catch(err => {
        console.log('[App] Error: ', err);
      });
  }

  /*
   * Call electron SQL and return a promise with it's result
   * Returns an ipcMainResponse
   */
  public execQuery(query: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const returnKey = this.key();

      this.electron.ipcRenderer.once(returnKey, (event, messages: ipcMainResponse) => {
        console.log('[App]: ' + returnKey);
        console.log('[App]: messages: ', messages);
        if (messages.success) {
          resolve(messages);
        } else {
          reject(messages);
        }
      });

      this.electron.ipcRenderer.send('SQL:execQuery', returnKey, query);
    });
  }

  /*
   * Generate a random key of 13 characters (ex: APP-54d1-d45c)
   * Used to ensure database returns do not replace each others
   */
  private key(): string {
    return 'APP-' + this.s4() + '-' + this.s4();
  }

  /*
   * Generate a random string of 4 characters (ex: 654s)
   */
  private s4(): string {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

}
