export interface ipcMainResponse {
  success: boolean;
  data: any;
  errorMessage: string;
}
