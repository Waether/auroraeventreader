/*
 * FCT_GameLog
 */

export interface GameLog {
  EventType: number; // See eventType.ts
  GameID: number;
  IDType: number;
  IncrementID: number;
  MessageText: string;
  PopulationID: number;
  RaceID: number;
  SMOnly: boolean;
  SystemID: number;
  Time: number; // UTC
  Xcor: number;
  Ycor: number;
}
