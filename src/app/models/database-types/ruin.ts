/*
 * DIM_Ruin
 */

export interface Ruin {
  AnnualTechChance: number;
  DefenceBases: number;
  Description: string;
  ExploitedChance: number;
  FactoriesBase: number;
  FactoriesRandom: number;
  FixedDSTS: number;
  Fleet: number;
  MaxChance: number;
  OffenceBases: number;
  Patrol: number;
  RandomDSTS: number;
  Regiment: number;
  RuinID: number;
  STO: number;
  Squadron: number;
}
