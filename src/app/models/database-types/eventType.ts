/*
 * DIM_EventType
 */

export interface EventType {
  AIInterupt: boolean;
  AttackEvent: boolean;
  CombatDisplay: number;
  DamageDisplay: number;
  Description: string;
  EventTypeID: number;
  PlayerInterupt: boolean;
}
