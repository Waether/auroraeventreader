export class ipcMainResponse {
  success: boolean;
  data: any;
  errorMessage: string;
}
