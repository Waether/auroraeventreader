"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.api = void 0;
var database_reader_1 = require("./database-reader/database-reader");
var Api = /** @class */ (function () {
    function Api() {
    }
    Api.prototype.initialize = function () {
        database_reader_1.databaseReader.initialize();
    };
    return Api;
}());
var api = new Api();
exports.api = api;
//# sourceMappingURL=api.js.map