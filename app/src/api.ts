import { databaseReader } from './database-reader/database-reader';

class Api {

  constructor() { }

  public initialize(): void {
    databaseReader.initialize();
  }
}

const api = new Api();
export { api };
