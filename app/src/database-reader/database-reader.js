"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.databaseReader = void 0;
var electron = require('electron');
var sqlite3 = require("sqlite3");
var sqlite_1 = require("sqlite");
var ipcMainResponse_1 = require("../models/ipcMainResponse");
var DatabaseReader = /** @class */ (function () {
    function DatabaseReader() {
        this.ipcMain = electron.ipcMain;
    }
    DatabaseReader.prototype.initialize = function () {
        var _this = this;
        console.log('filename: ', electron.app.getAppPath() + '\\AuroraDB.db');
        sqlite_1.open({
            filename: electron.app.getAppPath() + '\\AuroraDB.db',
            driver: sqlite3.Database
        })
            .then(function (db) {
            _this.database = db;
            _this.initEvents();
        })
            .catch(function (err) {
            console.error(err);
        });
    };
    DatabaseReader.prototype.initEvents = function () {
        var _this = this;
        this.ipcMain.on('SQL:execQuery', function (event, target, query) {
            var response = new ipcMainResponse_1.ipcMainResponse();
            _this.execQuery(query)
                .then(function (result) {
                response.success = true;
                response.data = result;
                event.sender.send(target, response);
            })
                .catch(function (err) {
                response.success = false;
                response.errorMessage = err;
                event.sender.send(target, response);
            });
        });
    };
    DatabaseReader.prototype.execQuery = function (query) {
        return this.database.all(query);
    };
    return DatabaseReader;
}());
var databaseReader = new DatabaseReader();
exports.databaseReader = databaseReader;
//# sourceMappingURL=database-reader.js.map