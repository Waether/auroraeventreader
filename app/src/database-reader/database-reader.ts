const electron = require('electron');


import * as sqlite3 from 'sqlite3';
import { open, Database } from 'sqlite';

import { ipcMainResponse } from '../models/ipcMainResponse';

class DatabaseReader {
  ipcMain = electron.ipcMain;
  database: Database<sqlite3.Database, sqlite3.Statement>;

  constructor() {
  }

  public initialize(): void {
    console.log('filename: ', electron.app.getAppPath() + '\\AuroraDB.db');
    open({
      filename: electron.app.getAppPath() + '\\AuroraDB.db',
      driver: sqlite3.Database
    })
      .then((db: Database<sqlite3.Database, sqlite3.Statement>) => {
        this.database = db;
        this.initEvents();
      })
      .catch(err => {
        console.error(err);
      });
  }

  private initEvents() {
    this.ipcMain.on('SQL:execQuery', (event: Electron.IpcMainEvent, target: string, query: string) => {
      let response = new ipcMainResponse();

      this.execQuery(query)
        .then(result => {
          response.success = true;
          response.data = result;

          event.sender.send(target, response);
        })
        .catch(err => {
          response.success = false;
          response.errorMessage = err;

          event.sender.send(target, response);
        });
    });
  }

  private execQuery(query: string): Promise<any[]> {
    return this.database.all(query);
  }
}

const databaseReader = new DatabaseReader();
export { databaseReader };
